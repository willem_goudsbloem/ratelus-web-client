angular.module("user.new.services", [])
    .factory("user.new.service", ["$http", "com.ratelus.config", function($http, config) {
        return {
          update: function(resp) {
            var email = '';
            if (resp.emails) {
              resp.emails.forEach(function(e) {
                if (e.type === "account") {
                  email = e.value;
                }
              });
            }
            var id = btoa(email);
            $http.put(config.user_api + "/" + id, resp)
                .success(function(data, status) {
                  
                })
                .error(function(data, status) {
                  id = 0;
                });
            return id;
          }
        };
      }]);


angular.module("user.new.controllers", [])
    .controller("user.new.controller", ["$scope", "$cookies", "user.new.service", function($scope, $cookies, service) {
        console.debug(":"+$cookies.id);
        if ($cookies.id === undefined) {
          $scope.session = true;
        } else {
          $scope.session = false;
        }
        $scope.$on('event:google-plus-signin-success', function(event, result) {

          if (result.status.signed_in && gapi.client.plus !== undefined) {
            gapi.client.plus.people.get(
                {'userId': 'me'})
                .execute(function(resp) {
                  var id = service.update(resp);
                  if (id !== 0) {
                    $cookies.id = id; 
                  }
                });

          }
        });
        $scope.$on('event:google-plus-signin-failure', function(event, authResult) {
          console.debug("not logged in");
        });
      }]);

angular.module("add.rating.directives", [])
    .directive("ratingStars", function() {
        return {
          restrict: "A",
          templateUrl: 'add/rating/add.rating.html',
          replace: true
      };
    });
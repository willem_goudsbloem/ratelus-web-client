angular.module("add.places.directives", [])
    .directive("searchType", ['$http', 'com.ratelus.config', function($http, config) {
        return {
          restrict: "A",
          template: '<input />',
          replace: true,
          link: function(scope, element, attrs) {
            $(element).select2({
              tags: [],
              minimumInputLength: 3,
              maximumSelectionSize: 3,
              query: function(query) {
                $http.get(config.api_search_organization_url + '/type', {
                  params: {
                    q: query.term
                  }
                }).then(function(res) {
                  var data = {results: []};
                  angular.forEach(res.data.organization_type, function(item) {
                    data.results.push({id: item.type, text: item.type});
                  });
                  query.callback(data);
                });
              }
            });
          }
        };
      }])
    .directive("searchTypeReadOnly", [function() {
        return {
          restrict: "A",
          template: '<input />',
          replace: true,
          link: function(scope, element, attrs) {
            attrs.$set("value", scope.place.type);
            $(element).select2({
              tags: [],
              multiple:'true'
            });
          }
        };
      }]);
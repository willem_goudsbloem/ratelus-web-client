angular.module("add.product.controllers", [])
    .controller("add.product.controller", ["$scope", "add.product.service", function($scope, product){
    $scope.product = {};
    $scope.product.year = new Date().getFullYear();
    $scope.search = product.search;
    $scope.rateProduct = function() {
          $scope.product.rating = $scope.rating;
          console.debug(angular.toJson($scope.product));
        };
}]);
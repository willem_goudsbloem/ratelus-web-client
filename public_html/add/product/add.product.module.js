angular.module("add.product", [
  "add.product.controllers",
  "add.product.services",
  "add.rating.directives"
]);
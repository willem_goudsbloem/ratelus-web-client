angular.module("add.product.services", [])
    .factory("add.product.service", ["$q", "$http", "com.ratelus.config", function($q, $http, config) {
        return {
          search: function(val, field) {
            return $http.get(config.api_search_product_url + '/' + field, {
              params: {
                q: val
              }
            })
                .then(function(res) {
                  var vals = [];
                  angular.forEach(res.data.product, function(item) {
                    vals.push(item[field]);
                  });
                  return vals;
                });
          }
        };
      }]);
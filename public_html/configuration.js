angular.module("ratelus.config", [])
    .constant('com.ratelus.config', {
      //search_api: "http://ratelus-search-api.herokuapp.com/api/search",
  //search_api: "http://localhost:8081/api/search"
      user_api: "http://ratelus-rest-api.herokuapp.com/api/rest/user",
    //user_api: "http://localhost:8082/api/rest/user",
    api_search_organization_url: "http://ratelus-search-api.herokuapp.com/api/search/organization",
    //api_search_organization_url: "http://localhost:8081/api/search/organization",
    api_search_product_url: "http://ratelus-search-api.herokuapp.com/api/search/product",
    //api_search_product_url: "http://localhost:8081/api/search/product",
    //api_product_url: "http://localhost:8081/api/product",
    api_product_url: "http://ratelus-search-api.herokuapp.com/api/product",
    //api_search_countries_url: "http://localhost:8081/api/search/countries"
    api_search_countries_url: "http://ratelus-search-api.herokuapp.com/api/search/countries",
    //api_place_url: "http://localhost:8081/api/place"
    api_place_url: "http://ratelus-search-api.herokuapp.com/api/place" 
    });
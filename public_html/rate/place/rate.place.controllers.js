angular.module("rate.place.controllers", [])
    .controller("rate.place.controller", ["$scope", "$routeParams", "$location", "rate.place.service", function($scope, $routeParams, $location, service) {
        $scope.place = {};
        $scope.rating = {};
        $scope.disabled = {};
        $scope.glyphicon = {};
        $scope.style = {};
        $scope.showParsedResult = false;
        $scope.showNearby = true;

        if ($routeParams.placeId) {
          service.getPlace($routeParams.placeId).then(function(place){
            parseResult(place, $scope);
            $scope.showNearby = false;
            $scope.showParsedResult = true;
          });
        }
        
        // Create the autocomplete object, restricting the search
        // to geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['establishment']});
        // When the user selects an address from the dropdown,
        // populate the address fields in the form.
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
          var result = autocomplete.getPlace();
          console.debug(angular.toJson(result));


          parseResult(result, $scope);

          $scope.glyphicon.icon = 'clear';
          $scope.showParsedResult = true;
          $scope.$apply();
        });

        $scope.ratePlace = function() {
          //assign rating to place
          $scope.place.rating = $scope.rating;
          console.debug(angular.toJson($scope.place));
          service.submit($scope.place);
          $scope.place = undefined;
          $location.path('/rate/success');
        };

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
        $scope.geolocate = function() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var geolocation = new google.maps.LatLng(
                  position.coords.latitude, position.coords.longitude);
              autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
                  geolocation));
            });
          }
        };

        $scope.clearBox = function() {
          $scope.google = '';
          $scope.place = {};
          $scope.disabled = {};
        };

        $scope.styleOnchange = function() {
          if ($scope.place.type.length > 0) {
            $("#s2id_autogen1").removeClass("ng-invalid2");
          }
        };

        function parseResult(result, $scope) {
          if (result.address_components !== undefined) {
            for (var i = 0; i < result.address_components.length; i++) {
              for (var j = 0; j < result.address_components[i].types.length; j++) {
                if (result.address_components[i].types[j] === "country") {
                  $scope.place.country = result.address_components[i].long_name;
                  $scope.disabled.country = true;
                }
                if (result.address_components[i].types[j] === "administrative_area_level_1") {
                  $scope.place.state = result.address_components[i].long_name;
                  $scope.disabled.state = true;
                }
                if (result.address_components[i].types[j] === "administrative_area_level_2") {
                  $scope.place.township = result.address_components[i].long_name;
                }
                if (result.address_components[i].types[j] === "locality") {
                  $scope.place.city = result.address_components[i].long_name;
                  $scope.disabled.city = true;
                }
                if (result.address_components[i].types[j] === "sublocality") {
                  $scope.place.sublocal = result.address_components[i].long_name;
                }
                if (result.address_components[i].types[j] === "route") {
                  $scope.place.street = result.address_components[i].long_name;
                  $scope.disabled.street = true;
                }
                if (result.address_components[i].types[j] === "postal_code") {
                  $scope.place.zip = result.address_components[i].long_name;
                }
                if (result.address_components[i].types[j] === "street_number") {
                  $scope.place.street_number = result.address_components[i].long_name;
                }
              }
            }
            if ($scope.place.city === undefined) {
              $scope.place.city = ($scope.place.township === undefined) ? $scope.place.sublocal : $scope.place.township;
              $scope.disabled.city = true;
            }
            if ($scope.place.street_number !== undefined) {
              $scope.place.street = $scope.place.street_number + " " + $scope.place.street;
            }
            $scope.place.name = result.name;
            $scope.disabled.name = true;
            $scope.disabled.type = true;
            $scope.place.type = result.types.join();
          } else {
            $scope.place.name = $scope.google;
          }
        }

      }]);


angular.module("rate.place", [
  "rate.place.controllers",
  "rate.place.directives",
  "add.rating.directives",
  "rate.place.services"
]);
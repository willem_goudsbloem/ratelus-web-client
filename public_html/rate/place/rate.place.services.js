angular.module('rate.place.services', [])
    .factory('rate.place.service', ["$http", "$q", 'com.ratelus.config', function($http, $q, config) {
        return {
          submit: function(place) {
            $http.put(config.api_place_url, {"place": place});
          },
          getPlace: function(place_id) {
            var q = $q.defer();
            navigator.geolocation.getCurrentPosition(function(position) {
              loc = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              gservice = new google.maps.places.PlacesService(new google.maps.Map(document.createElement('map'), {
                center: loc,
                zoom: 15
              }));
              gservice.getDetails({placeId: place_id}, function(result, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                //console.debug(angular.toJson(result));                  
                  q.resolve(result);
                }
              });
            });
            return q.promise;
          }
        };
      }]);

angular.module('rate.product.controllers', [])
    .controller('rate.product.controller', ["$scope", "$location", "rate.product.service", function($scope, $location, service) {
        $scope.product = {};
        $scope.valid = {};
        var date = new Date();
        $scope.valid.maxYear = date.getFullYear() + 2;
        $scope.rateProduct = function() {
          $scope.product.rating = $scope.rating;
          console.debug(angular.toJson($scope.product));
          service.submit($scope.product);
          $scope.product = undefined;
          $location.path('/rate/success');
        };
      }]);

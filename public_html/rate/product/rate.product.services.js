angular.module('rate.product.services', [])
    .factory('rate.product.service', ["$http", 'com.ratelus.config', function($http, config) {
        return {
          submit: function(product){
            $http.put(config.api_product_url + "/" + product.brand + "/" + product.model, {"product":product});
          }
        };
      }]);

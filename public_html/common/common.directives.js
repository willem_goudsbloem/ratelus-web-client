angular.module("common.directives", [])
    .directive("spinnerGlyphicon", [function() {
        return {
          restrict: 'A',
          template: '<i class="icon-spin icon-refresh"></i>',
          replace: true
        };
      }])
    .directive('focus', function() {
    var timer;
    return function(scope, elm, attr) {
        if (timer) clearTimeout(timer);    
        timer = setTimeout(function() {
            elm.focus();
        }, 0);
    };
})
.directive('capitalizeFirst', function() {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue===undefined) return;
           var capitalized = inputValue.charAt(0).toUpperCase() + inputValue.substring(1);
           if(capitalized !== inputValue) {
              modelCtrl.$setViewValue(capitalized);
              modelCtrl.$render();
            }         
            return capitalized;
         };
         modelCtrl.$parsers.push(capitalize);
         capitalize(scope[attrs.ngModel]);
     }
   };
});



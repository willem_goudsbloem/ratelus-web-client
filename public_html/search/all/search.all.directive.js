angular.module("search.all.directives", [])
    .directive("searchAllDirective", [function() {
        return {
          restrict: 'C',
          templateUrl: 'search/all/search.all.directive.html',
          replace: false
        };
      }]);



angular.module("search.all.services", []).
    factory("search.all.service", [
      "$q",
      "$http",
      "com.ratelus.config",
      function($q, $http, config) {
        return {
          search: function(searchText) {
            return $q.all([
              $http.get(config.api_search_organization_url + "/name", {
                params: {
                  q: searchText
                }
              }),
              $http.get(config.api_search_product_url, {
                params: {
                  q: searchText
                }
              })
            ]);
          }
        };
      }]);



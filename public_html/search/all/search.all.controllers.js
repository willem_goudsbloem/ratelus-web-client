angular.module("search.all.controllers", [])
    .controller("search.all.controller", ["$scope", "search.all.service", function($scope, service) {
        $scope.search = function() {
          // service.search returns a promise
          service.search($scope.searchText)
              .then(function(results) {
                $scope.agg = [];
                angular.forEach(results, function(result) {
                  //console.debug(angular.toJson(result));
                  if (result.data.organization_name) {
                    result.data.organization_name.forEach(function(org) {
                      var extra = org.location.city + ", " + org.location.state;
                      $scope.agg.push({"name": org.name, "extra": extra, "type": org.organization_type, "rating" : org.rating});
                    });
                  }
                  if (result.data.product) {
                    result.data.product.forEach(function(prod) {
                      //console.debug(":" + prod.brand);
                      var name = prod.brand + " " + prod.model;
                      $scope.agg.push({"name": name, "extra": prod.year, "type": prod.type, "rating" : prod.rating});
                    });
                  }
                });
              });
        };
        $scope.stars = [1,2,3,4,5];
      }]);



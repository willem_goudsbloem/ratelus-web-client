angular.module("search.place.nearby.services", []).
    factory("search.place.nearby.service", ["$q", function($q) {

        var loc;

        return  {
          getService: function() {
            var q = $q.defer();
            navigator.geolocation.getCurrentPosition(function(position) {
              loc = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              q.resolve(new google.maps.places.PlacesService(new google.maps.Map(document.createElement('map'), {
                center: loc,
                zoom: 15
              })));
            });
            return q.promise;
          },
          getPlaces: function(service) {
            var q = $q.defer();
            var request = {
              location: loc,
              radius: '50',
              types: ['establishment']
            };
            service.nearbySearch(request, function(results, status) {
              if (status == google.maps.places.PlacesServiceStatus.OK) {
                q.resolve(results);
              }
            });
            return q.promise;
          }

        };

      }]);



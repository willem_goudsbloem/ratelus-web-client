angular.module("search.place.nearby.controllers", [])
    .controller("search.place.nearby.controller", ["$scope", "search.place.nearby.service", function($scope, service) {
        serv = service.getService();
        serv.then(function(gservice) {
          service.getPlaces(gservice).then(function(places) {
            $scope.places = places;
          }
          );
        });

      }]);



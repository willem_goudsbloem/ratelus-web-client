angular.module("search.organization.type.directive", [])
    .directive("searchOrganizationType", [function() {
        return {
          restrict: 'A',
          templateUrl: 'search/organization/type/search.organization.type.html',
          replace: false
        };
      }]);
angular.module("search.organization.type.services", [])
    .service("search.organization.type.service", ['$http', 'com.ratelus.config', function($http, config) {
        return function(val) {
          return $http.get(config.api_search_organization_url + '/type', {
            params: {
              q: val
            }
          }).then(function(res) {
            var types = [];
            angular.forEach(res.data.organization_type, function(item) {
              types.push(item.type);
            });
            return types;
          });
        };
      }]);
angular.module("search.organization.name.services", [])
    .service("search.organization.name.service", ['$http', 'com.ratelus.config', function($http, config) {
        return function(val) {
          return $http.get(config.search_api + '/organization/name', {
            params: {
              q: val
            }
          }).then(function(res) {
            var results = [];
            angular.forEach(res.data.result, function(item) {
              results.push(item.name);
            });
            return results;
          });
        };
      }]);
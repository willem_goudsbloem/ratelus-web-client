angular.module("search.organization.name.controllers", [])
    .controller("search.organization.name.controller", ["$scope", "search.organization.name.service", function($scope, service){
      $scope.getSearchResult = service;
}]);
angular.module("search.product.model.directives", [])
    .directive("searchProductModel", [function() {
        return {
          restrict: 'A',
          templateUrl: 'search/product/model/search.product.model.html',
          replace: true
        };
      }]);
angular.module("search.product.model.services", [])
    .service("search.product.model.service", ['$http', 'com.ratelus.config', function($http, config) {
        return function(model, brand) {
          //console.debug("i" + brand + ":" + val);
          return $http.get(config.api_search_product_url + '/model', {
            params: {
              brand: brand,
              model: model
            }
          }).then(function(res) {
            var products = [];
            angular.forEach(res.data.product, function(item) {
              products.push(item);
            });
            return products;
          });
        };
      }]);




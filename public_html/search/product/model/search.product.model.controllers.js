angular.module('search.product.model.controllers', [])
    .controller('search.product.model.controller', ["$scope", "search.product.model.service", function($scope, search) {
        $scope.getProducts = function(val) {
          $scope.products = search(val, $scope.product.brand);
          return $scope.products;
        };
      }]);

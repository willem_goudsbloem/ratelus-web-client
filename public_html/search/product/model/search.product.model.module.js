angular.module("search.product.model", [
  'search.product.model.controllers',
  'search.product.model.services',
  'search.product.model.directives'
]);
angular.module("search.product.brand.services", [])
    .service("search.product.brand.service", ['$http', 'com.ratelus.config', function($http, config) {
        return function(val) {
          return $http.get(config.api_search_product_url + '/brand', {
            params: {
              q: val
            }
          }).then(function(res) {
            var products = [];
            angular.forEach(res.data.product, function(item) {
              //console.info(item);
              products.push(item);
            });
            return products;
          });
        };
      }]);




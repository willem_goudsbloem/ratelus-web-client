angular.module("search.product.brand.directives", [])
    .directive("searchProductBrand", [function() {
        return {
          restrict: 'A',
          templateUrl: 'search/product/brand/search.product.brand.html',
          replace: true
        };
      }]);
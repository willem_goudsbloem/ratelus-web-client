angular.module("search.product.brand", [
  'search.product.brand.controllers',
  'search.product.brand.services',
  'search.product.brand.directives'
]);
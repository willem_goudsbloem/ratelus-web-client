angular.module("search.product.type.directives", [])
    .directive("searchProductType", function() {
      return {
        restrict: 'A',
        templateUrl: 'search/product/type/search.product.type.html',
        replace: true
      };
    });
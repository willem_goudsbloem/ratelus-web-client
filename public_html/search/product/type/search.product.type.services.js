angular.module("search.product.type.services", [])
    .service("search.product.type.service", ['$http', 'com.ratelus.config', function($http, config) {
        return function(val) {
          return $http.get(config.api_search_product_url + '/type', {
            params: {
              q: val
            }
          }).then(function(res) {
            var products = [];
            angular.forEach(res.data.product, function(item) {
              products.push(item);
            });
            return products; 
          });
        };
      }]);




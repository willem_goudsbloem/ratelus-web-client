angular.module("search.product.type", [
  'search.product.type.controllers',
  'search.product.type.services',
  'search.product.type.directives'
]);
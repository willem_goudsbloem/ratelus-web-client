angular.module("search.countries.directive", [])
    .directive("searchCountry", [function() {
        return {
          restrict: 'A',
          templateUrl: 'search/countries/search.countries.html',
          replace: false
        };
      }]);
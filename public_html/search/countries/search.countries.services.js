angular.module("search.countries.services", [])
    .service("search.countries.service", ['$http', 'com.ratelus.config', function($http, config) {
        return function(val) {
          console.debug("?" + val);
          return $http.get(config.api_search_countries_url, {
            params: {
              q: val
            }
          }).then(function(res) {
            var types = [];
            angular.forEach(res.data.countries, function(item) {
              types.push(item.name);
            });
            return types;
          });
        };
      }]);
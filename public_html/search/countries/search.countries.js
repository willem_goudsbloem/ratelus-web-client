angular.module("search.countries", [
  "search.countries.directive",
  "search.countries.controllers",
  "search.countries.services"
]);
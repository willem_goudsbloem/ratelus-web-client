angular.module("ratelus.route", ["ngRoute"])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/rate', {
              templateUrl: 'rate/rate.html'
            })
            .when('/rate/place', {
              templateUrl: 'rate/place/rate.place.html'
            })
            .when('/rate/product', {
              templateUrl: 'rate/product/rate.product.html'
            })
            .when('/rate/success', {
              templateUrl: 'rate/success/rate.success.html'
            })
            .when('/', {
              templateUrl: 'home/home.html'
            })           
            .otherwise({
              redirectTo: '/'
            });
      }]);
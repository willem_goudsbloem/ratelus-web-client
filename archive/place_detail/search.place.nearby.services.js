angular.module("search.place.nearby.services", []).
    factory("search.place.nearby.service", ["$q", function($q) {
        
          var loc;  
        
        return  {
         
          getService: function() {
            var q = $q.defer();
            navigator.geolocation.getCurrentPosition(function(position) {
              loc = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                       q.resolve(new google.maps.places.PlacesService(new google.maps.Map(document.createElement('map'), {
                center: loc,
                zoom: 15
              })));
            });
            return q.promise;
          },

          getPlaces: function(service) {
            var q = $q.defer();
            var request = {
              location: loc,
              radius: '500'
            };
            service.nearbySearch(request, function(results, status) {
              if (status == google.maps.places.PlacesServiceStatus.OK) {
                q.resolve(results);
              }
            });
            return q.promise;
          },

          getPlace: function(place, service) {
            var q = $q.defer();
            service.getDetails({placeId: place.place_id}, function(result, status) {
              if (status == google.maps.places.PlacesServiceStatus.OK) {
                q.resolve(result);
              }
            });
            return q.promise;
          }
          
        };

      }]);



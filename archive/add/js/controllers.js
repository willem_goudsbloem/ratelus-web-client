'use strict';

/* Controllers */

//add.angular.module('add.controllers', []).
//  controller('MyCtrl1', [function() {
//
//  }])
//  .controller('MyCtrl2', [function() {
//
//  }]);

angular.module('add.controllers', []);
  
  function objectiveCtrl($scope, $location, $http) {
	$http.get("../api/static/add.objectives.json").success(
			function(data, status, headers, config) {
				$scope.objectives = data;
			});
	$scope.changed = function(){
		$location.path($scope.objective.val);
	};
}

function productCtrl($scope, $http){
	$scope.rating_type = "product";
	$scope.max_year = new Date().getFullYear()+1;
	var cur = new Date().getFullYear();
	$scope.object = { product_year : cur };
}

function businessCtrl($scope, $http){
	$scope.rating_type = "business";
	//console.debug("businessCtrl")
	if (navigator.geolocation) {
	//	console.debug("supports geo loc");	
    navigator.geolocation.getCurrentPosition(function(position){
    	  $http.get("http://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&sensor=true").success(function(data, status, headers, config){
    		  //console.debug(data);
    		  var result = data.results[0];
    		  var country;
    		  var city;
                  var township;
    		  var state;
    		  var street;
    		  for (var i=0; i < result.address_components.length; i++) {
    			  for (var j=0; j<result.address_components[i].types.length; j++) {
    				  if (result.address_components[i].types[j] === "country") {
    					  country = result.address_components[i].long_name;
    				  } 
    				  if (result.address_components[i].types[j] === "administrative_area_level_1") {
    					  state = result.address_components[i].long_name;
    				  }
                                  if (result.address_components[i].types[j] === "administrative_area_level_2") {
    					  township = result.address_components[i].long_name;
    				  }
    				  if (result.address_components[i].types[j] === "locality") {
    					  city = result.address_components[i].long_name;
    				  }
    				  if (result.address_components[i].types[j] === "route") {
    					  street = result.address_components[i].long_name;
    				  }
    			  }
    		  }  		  
                  if (city===undefined) city = township;
    		  $scope.object = { business_country : country
    				  , business_city : city + ", " + state
    				  , business_address : street };
    	  })
    });
    } else {
    	console.debug("no support geo loc, go with IP");
    	$http.get("http://ipinfo.io/json").success(function(data, status, headers, config) {
    	    console.log(data);
    	    //$scope.object = { business_country : data }
    	});
    }
	$scope.clear = function(id){
		$scope.object[id] = "";
	}
}


function entertainmentCtrl($scope, $http){
}

function ratingCtrl($scope, $http, $rootScope) {
	$scope.submitAll = function(){
		$rootScope.message = "please wait";
		var str= "{";
		var b = 0;
		for (val in $scope.object) {
			if (b++>0) str+= ","; 
			str+="\"" + val + "\":\"" + $scope.object[val] + "\"";
		}
		str+="}";
		console.debug(str);
		$http.post("/rate/"+$scope.rating_type, str).success(
				function(data, status, headers, config) {
					$rootScope.message = data;		
				}).
				error(function(data, status, headers, config){
					$rootScope.message = data;
				});
	};
}

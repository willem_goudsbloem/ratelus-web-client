'use strict';

var add = angular.module('add', [
  'ngRoute',
  'add.filters',
  'add.services',
  'add.directives',
  'add.controllers'
]);

add.config(function($routeProvider) {
	$routeProvider.when("/product", {
		controller : productCtrl,
		templateUrl : 'partials/product.html',
		view : 'objective.pick'
	}).when("/business", {
		templateUrl : 'partials/business.html',
		controller : 'businessCtrl',
		view : 'objective.pick'
	}).when("/entertainment", {
		templateUrl : 'partials/entertainment.html',
		controller : 'entertainmentCtrl',
		view : 'objective.pick'
	});
	}
);


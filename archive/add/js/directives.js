'use strict';

var d = angular.module('add.directives', []);

/* Directives */
d.directive("capitalize", function(){
	 return {
	     require: 'ngModel',
	     link: function(scope, element, attrs, modelCtrl) {
	        var capitalize = function(inputValue) {
	        	if (inputValue == undefined) return;
	           var capitalized = inputValue.charAt(0).toUpperCase() + inputValue.substring(1).toLowerCase();
	           var capitalized = inputValue.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	           if(capitalized !== inputValue) {
	              modelCtrl.$setViewValue(capitalized);
	              modelCtrl.$render();
	            }         
	            return capitalized;
	         }
	         modelCtrl.$parsers.push(capitalize);
	         capitalize(scope[attrs.ngModel]);  // capitalize initial value
	     }
	   };
});

d.directive('rating', function ($compile, $http) {
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function() {
    		$http.get("partials/rating.html").success(
    				function(data, status, headers, config) {
    					ele.html(data);
    			        $compile(ele.contents())(scope);				
    				}); 
      });
    }
  };
});



module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    clean: {
      build: {
        src: ["dist/", "tmp/"]
      }
    },
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: ['public_html/**/*.js'],
        // the location of the resulting JS file
        dest: 'tmp/ratelus-min.js.tmp'
      }
    },
    copy: {
      main: {
        files: [
          {expand: true, src: ['public_html/**/*.html', 'public_html/**/*.png',],
            dest: 'dist/', filter: 'isFile'}
        ]
      },
      dev: {
         files: [
          {expand: false, src: ['tmp/ratelus-min.js.tmp'],
            dest: 'dist/public_html/ratelus-min.js', filter: 'isFile'}
        ]
      }
    },
    watch: {
      scripts: {
        files: ['public_html/**/*.*'],
        tasks: ['clean', 'jshint', 'cssmin', 'concat', 'copy', 'copy:dev'],
        options: {
          spawn: false
        }
      }
    },
    uglify: {
      build: {
        options: {
          mangle: true
        },
        files: {
          'dist/public_html/ratelus-min.js': ['tmp/ratelus-min.js.tmp']
        }
      }
    },
    jshint: {
      beforeconcat: ['public_html/**/*.js', 'public_html/**/*.json'],
      afterconcat: ['dist/**/*.js']
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true
        },
        files: {
          'dist/public_html/index.html': 'public_html/index.html'
        }
      }
    },
    cssmin: {
      combine: {
        files: {
          'dist/public_html/ratelus-min.css': ['public_html/**/*.css']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

grunt.registerTask('default', ['clean', 'jshint', 'concat', 'uglify', 'cssmin', 'copy:main', 'htmlmin:dist']);

};
 